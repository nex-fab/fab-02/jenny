# Summary

* [1.Project management](https://www.nexmaker.com/doc/1projectmanage/Assessment1project-manage.html)
  * [Introduce myself](1projectmanagement/introduce.md)
  * [final project](1projectmanagement/finalprojectplan.md)


* [2.cad design]()
  * [solidwork](2cad/solidwork.md)
* [3.3Dprinting]()
  * [3Dprintingcup](33Dprinting/Cup.md)
* [4.ArduinoApplication]()
  * [LEDlighting](4Arduinoapplication/LEDlighting.md)
  * [Project](4Arduinoapplication/project.md)
* [5.Lasercutting]()
  * [Design](5lasercutting/Design.md)


update 20200728
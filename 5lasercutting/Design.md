# Design for Laser Cutting 

## Step 1
首先我们打开AutoCAD建立实线，虚线和点划线的图层并设立线条不同颜色
![](https://gitlab.com/jennyius/ius/uploads/0ba3f8be4cd8e2208fe2eda9d03054a4/WechatIMG216.png)
![](https://gitlab.com/jennyius/ius/uploads/eca2230c8c0eac9f3a2e760668548624/20200827172655.png)
![](https://gitlab.com/jennyius/ius/uploads/b63dcd89578ecc6a2face0e43e855e3c/20200827172350.png)

## Step 2
用REC口令画出一个正方形，长度为90mm,以2.4mm的厚度用REC口令在外面画出另一个正方形
![](https://gitlab.com/jennyius/ius/uploads/5d77baabcf6b6f455383bc37a469ee79/20200827172550.png)

## Step 3
在正方形中，画出中心线
![](https://gitlab.com/jennyius/ius/uploads/f1589783f8613cb5e81ed06ab5b9a8c7/20200827173023.png)

## Step 4
用M口令将正方形分成长度相同的三部分
![](https://gitlab.com/jennyius/ius/uploads/172b96a78b7876bed345f0f8094722a5/20200827173159.png)

## Step 5
用TR口令删掉不需要的线条，用MA口令将虚线都变成白色的实线
![](https://gitlab.com/jennyius/ius/uploads/a1fdbe9f97e4be2f3409cf22812032af/20200827173420.png)
![](https://gitlab.com/jennyius/ius/uploads/306afc3becd647a040d1e404ce4858b4/20200827173516.png)

## Step 6
复制粘贴出一块新的板
![](https://gitlab.com/jennyius/ius/uploads/ac968459404b90d3f42d5e5c363544f1/20200827173854.png)

## Step 7
用"MI" 口令做镜像
![](https://gitlab.com/jennyius/ius/uploads/93925538b52caffab2e25947256b83a1/20200827174146.png)

## Step 8
[导出文件]（https://drive.google.com/file/d/1U8TehabBrsu2LkJLOrlYyB0NoW70sDZd/view?usp=sharing）

## Step 9
画圆

![](https://gitlab.com/jennyius/ius/uploads/325238671d7b9d3d4c48c3d62e1e3ef3/20200901143125.png)

## Step 10 
Laser cutting 
![](https://gitlab.com/jennyius/ius/uploads/49288f13cae48a4a35c77d049f517465/WechatIMG238.jpeg)

## Step 11
组装

![](https://gitlab.com/jennyius/ius/uploads/69a5611a678e051eed1df8c89bfed61f/WechatIMG237.jpeg)
![](https://gitlab.com/jennyius/ius/uploads/7195bc64a1fd20b33a7f716886db7257/WechatIMG236.jpeg)

## 成品
![](https://gitlab.com/jennyius/ius/uploads/162487040152206a8c3cd3cbc2113969/WechatIMG234.jpeg)


<iframe width="560" height="315" src="https://www.youtube.com/embed/ISq34ubK0LU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


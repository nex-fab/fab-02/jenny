# 感光灯
## 项目介绍
我们这次做的这个非常好玩，叫做感光灯。它能随着光线明暗而选择是否亮灯。这个光感灯非常适合用做夜晚使用的小夜灯。晚上睡觉的时候，家中灯关掉后，感光灯感觉到周围环境变暗了，就自动亮起。到了白天，天亮后，感光灯就又恢复到关闭的状态了。本次项目用3个led灯模拟在不同灯光条件下LED的发光情况。
## 所需材料
3×  5mm LED灯
1×  220欧电阻
1×  10k电阻
3×  220欧电阻
1×  光敏二极管
1×  手电筒（可选）

## 项目步骤
Step 1 代码书写

```
int LED1 = 13;                     //设置LED灯为数字引脚13
int LED2 = 12;                     //设置LED灯为数字引脚13
int LED3 = 11;                     //设置LED灯为数字引脚13
int val = 0;                      //设置模拟引脚0读取光敏二极管的电压值

void setup(){
      pinMode(LED1,OUTPUT); 
      pinMode(LED2,OUTPUT);
      pinMode(LED3,OUTPUT);// LED为输出模式
      Serial.begin(9600);        // 串口波特率设置为9600
}

void loop(){
      val = analogRead(0);         // 读取电压值0~1023
      Serial.println(val);         // 串口查看电压值的变化
      if(val<200){                // 一旦小于设定的值，LED灯关闭
              digitalWrite(LED1,LOW);
              digitalWrite(LED2,HIGH);
              digitalWrite(LED1,LOW);
      }else if(val>200 & val<700){                        // 否则LED亮起
              digitalWrite(LED1,HIGH);
              digitalWrite(LED2,LOW);
              digitalWrite(LED3,LOW);
      }else{                        // 否则LED亮起
              digitalWrite(LED1,HIGH);
              digitalWrite(LED2,HIGH);
              digitalWrite(LED3,HIGH);
      delay(10);                   // 延时10ms
      }
}
```

Step 2 线路连接
![](https://gitlab.com/jennyius/ius/uploads/07b39e4ab2c5dcaf5ed2601e96bde61d/20200825134741.png)

![](https://gitlab.com/jennyius/ius/uploads/31c23bc1dda1a6da6f32c029d3bcfde5/20200825124036.png)

Step 3 代码验证
![](https://gitlab.com/jennyius/ius/uploads/4340275881b956edfecc03ae895cdbfe/20200825124334.png)

Step 4 端口确认
![](https://gitlab.com/jennyius/ius/uploads/46591f3b7b1e83fe11adeacf27739931/20200825124422.png)

Step 5 文件上传保存
![](https://gitlab.com/jennyius/ius/uploads/3e47529b06e7af36df9047ad71d39067/20200825124602.png)

Step 6 光敏数据测试
![](https://gitlab.com/jennyius/ius/uploads/94c6d4d3b2ec10f98a4927e44d10777e/20200825124712.png)

Step 7 跑项目

<iframe width="560" height="315" src="https://www.youtube.com/embed/Gq289B0b7yA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



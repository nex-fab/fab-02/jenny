#  LED light Water Control 

## Step1 Coding 
```
void setup() {
pinMode(13, OUTPUT);
pinMode(12, OUTPUT);
pinMode(11, OUTPUT);
}

void loop() {
digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)
  
digitalWrite(13, LOW);
digitalWrite(12, HIGH);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, LOW);
digitalWrite(12, LOW);
digitalWrite(11, HIGH);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, HIGH);
delay(200); //Wait for 200 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, HIGH);
digitalWrite(11, LOW);
delay(2000); //Wait for 2000 millisecond(s)
  
}
```
check the code if right and save the file 

## Step2 Conecting the components together 
![](https://gitlab.com/jennyius/ius/uploads/1c40d782305680d64e840c90bb904ab4/20200818173447.png)

## Step3 Conect device with computer 
![](https://gitlab.com/jennyius/ius/uploads/d46bbd4bebfbc270f427e948124aadc9/20200818173705.png)
![](https://gitlab.com/jennyius/ius/uploads/a9d60d90feead934eb2857083fe4b789/%E6%B5%81%E6%B0%B4%E7%81%AF.gif)
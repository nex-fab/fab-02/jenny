# Class for Solidworks 

##  In last class we learnt how to create a cup design by using soliworks software. 

## STEP.1 draw a line and cicle 
![](https://gitlab.com/jennyius/ius/uploads/432b17f362bde4dcda15bc79cf4b966f/20200804122642.png)

![](https://gitlab.com/jennyius/ius/uploads/0b449c3d52c8ffe6e3b040dae021635c/20200804122729.png)

## STEP.2 stretch and set pull grinding angle
![](https://gitlab.com/jennyius/ius/uploads/9c612e0c389655e03516a914ff0d7908/20200804122951.png)

![](https://gitlab.com/jennyius/ius/uploads/875a895c601eefc77add0bcf095f7052/20200804123226.png)

![](https://gitlab.com/jennyius/ius/uploads/875a895c601eefc77add0bcf095f7052/20200804123226.png)

## STEP.3 Set wall thickness of cup 
![](https://gitlab.com/jennyius/ius/uploads/983649013ac5b656315fc5b4732276d9/20200804123528.png)

![](https://gitlab.com/jennyius/ius/uploads/d56c5f535079ab869aff57714a45f317/20200804123559.png)

## STEP.4 Make cover
![](https://gitlab.com/jennyius/ius/uploads/e13891aa8cc33c90cc378cb85793b75d/20200804123803.png)

![](https://gitlab.com/jennyius/ius/uploads/18106d835d2b7685d12ccc99ab7ff147/20200804123842.png)

![](https://gitlab.com/jennyius/ius/uploads/87b66aabdd635729bed8283628117ec7/20200804123917.png)

![](https://gitlab.com/jennyius/ius/uploads/5b503eef2534eacad799bc9b601bf2f1/20200804123948.png)

## STEP.5 Combine together 
![](https://gitlab.com/jennyius/ius/uploads/2433da0f5e34cb72c8444fbb27e28b6a/20200804124104.png)

![](https://gitlab.com/jennyius/ius/uploads/09a8fb8344e0cf6b069c820a316aa3b9/20200804124155.png)

## Document 
https://drive.google.com/drive/folders/13JrjvTj6ZpqTnCxa42t5OUg4NEqsbNBz


It is a nice learning experience for me. I am so expecting to make my cup in real thing through 3D printing. 






